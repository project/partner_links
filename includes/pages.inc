<?php

/**
 * @file
 * Menu callbacks.
 */

/**
 * It edits a specified partner.
 */
function partner_links_edit_partner($form, &$form_state, $pid) {
  $form = array();

  $form['#partner'] = _partner_links_load_partner_by_id($pid);

  $form['partner_name'] = array(
    '#default_value' => $form['#partner']['partner_name'],
    '#type' => 'textfield',
    '#title' => t('Partner Name'),
    '#required' => TRUE,
  );

  $form['partner_url'] = array(
    '#default_value' => $form['#partner']['partner_url'],
    '#type' => 'textfield',
    '#title' => t('Partner URL'),
    '#required' => TRUE,
  );

  return confirm_form($form,
    t('Are you sure you want to edit %title?',
      array('%title' => $form['#partner']['partner_name'])),
    PARTNER_LINKS_DEFAULT_PATH,
    '',
    t('Save'),
    t('Cancel')
  );
}

/**
 * Submit handler of partner edit form.
 */
function partner_links_edit_partner_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $partner_name = $form_state['values']['partner_name'];
    $partner_url = $form_state['values']['partner_url'];
    $pid = $form['#partner']['pid'];

    $data = array(
      'partner_name'  => $partner_name,
      'partner_url'  => $partner_url,
    );

    db_update(PARTNER_LINKS_DB_TABLE)
      ->fields($data)
      ->condition('pid', $pid, '=')
      ->execute();

    drupal_set_message(t('%name has been updated.',
      array('%name' => $form['#partner']['partner_name'])));
  }

  $form_state['redirect'] = PARTNER_LINKS_DEFAULT_PATH;
}

/**
 * Validates the values of the form.
 */
function partner_links_edit_partner_validate($form, &$form_state) {
  _partner_links_check_partner_link($form_state['values']['partner_url'],
    'partner_url');
}

/**
 * It deletes a specified partner.
 */
function partner_links_delete_partner($form, &$form_state, $pid) {
  $form['#partner'] = _partner_links_load_partner_by_id($pid);

  return confirm_form($form,
    t('Are you sure you want to delete %title?',
      array('%title' => $form['#partner']['partner_name'])),
    PARTNER_LINKS_DEFAULT_PATH,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler of partner delete form.
 */
function partner_links_delete_partner_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_delete(PARTNER_LINKS_DB_TABLE)
      ->condition('pid', $form['#partner']['pid'], '=')
      ->execute();

    drupal_set_message(t('%name has been deleted.',
      array('%name' => $form['#partner']['partner_name'])));
  }
  $form_state['redirect'] = PARTNER_LINKS_DEFAULT_PATH;
}

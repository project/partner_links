## Introduction

It is a linkexchange module.  
This module provides a block where you can show the links of your partner sites.  
It also gives you an admin interface, you can add/delete/remove your partners.  
At every cron run the module checks the link of your site  
on the given partner sites.  
Currently we only support Drupal 7, but porting to D8 is in progress.  

## Requirements

* Core Block module

## Installation

On the traditional drupal way  
(https://www.drupal.org/documentation/install/modules-themes/modules-7)

## Configuration

With admin role on the admin/structure/partner-links page  
Here you can manage your partners.

## Maintainers

@junkuncz https://www.drupal.org/u/junkuncz

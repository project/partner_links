<?php

/**
 * @file
 * Theme of the partner links block.
 *
 * - $partners: The partner object.
 *   It has partner_name and partner_url properties.
 *
 * @var $partners.
 */
?>
<ul class="partner-links">
  <?php foreach ($partners as $partner): ?>
      <li class="partner-name">
        <a class="partner-link" href="<?php print $partner['partner_url']; ?>" target="_blank">
          <?php print $partner['partner_name']; ?>
        </a>
      </li>
  <?php endforeach; ?>
</ul>
